from django.urls import path
from accounts.views import loginUser, logoutUser, signUpUser

urlpatterns = [
    path("login/", loginUser, name="login"),
    path("logout/", logoutUser, name="logout"),
    path("signup/", signUpUser, name="signup"),
]
